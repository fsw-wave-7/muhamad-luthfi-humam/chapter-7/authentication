// router.js
const router = require('express').Router();

const passport = require('./lib/passport')

// Controllers
const auth = require('./controllers/authControllers');

const restrict = require('./middlewares/restrict')

const restrictApi = require('./middlewares/restrict-api')

const bodyParser = require('body-parser')


router.get('/', restrict, (req, res) => res.render('index'))

// Homepage
router.get('/', (req, res) => res.render('index'));

// Register Page
router.get('/register', (req, res) => res.render('register'));
router.post('/register', auth.register);

// router.js
router.get('/login', (req, res) => res.render('login'));

router.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true,
  })
);

router.get('/whoami', restrict, auth.whoami)

router.use(bodyParser.json())

// Login Page
router.post('/api/v1/auth/login', auth.login)
router.get('/api/v1/auth/whoami', restrictApi, auth.whoamiApi)

module.exports = router;
